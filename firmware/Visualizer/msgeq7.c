/**
 * \file msgeq7.c
 * \author Ethan Ruffing
 * \since 2017-07-11
 */ 

#include <stdlib.h>
#include <avr/io.h>

#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#include <util/delay.h>

#include "msgeq7.h"

Msgeq7 msgeq7Init(volatile uint8_t *strobeport, volatile uint8_t *strobeddr,
                  uint8_t strobepin, volatile uint8_t *resetport,
                  volatile uint8_t *resetddr, uint8_t resetpin,
                  uint8_t outputadc) {
    Msgeq7 eq = malloc(sizeof(struct msgeq7));
	eq->strobeport = strobeport;
	eq->strobeddr = strobeddr;
	eq->strobepin = strobepin;
	eq->resetport = resetport;
	eq->resetddr = resetddr;
	eq->resetpin = resetpin;
	eq->outputadc = outputadc;
	*eq->strobeddr |= _BV(eq->strobepin);
	*eq->resetddr |= _BV(eq->resetpin);
	*eq->strobeport |= _BV(eq->strobepin);
	_delay_us(100);
	*eq->resetport |= _BV(eq->resetpin);
	_delay_us(100);
	*eq->resetport &= ~_BV(eq->resetpin);
	_delay_us(100);
	
	// Turn on the ADC and set prescaler to divide-by-128
	ADCSRA = 0;
	ADCSRA = _BV(ADEN) | _BV(ADPS1);
	
	// Set the reference voltage to AVcc
	ADMUX |= _BV(REFS0);
	ADMUX |= _BV(REFS1);
	
	// Set the ADC Mux
	ADMUX |= eq->outputadc;
	
	// Set for 10-bit conversion
	ADMUX &= ~_BV(ADLAR);
	
	return eq;
}

/**
 * @var refVoltage
 *     The voltage at which the microcontroller is running (AVcc).
 */
static uint16_t refVoltage = 5.0;

/**
 * Read the ADC value.
 *
 * \return The raw value from the ADC.
 */
uint16_t readAnalog() {
	ADCSRA |= _BV(ADIF);	// Clear the ADC interrupt flag.
	ADCSRA |= _BV(ADSC);	// Start the next conversion
	while (!(ADCSRA & _BV(ADIF)));	// Wait for conversion to complete.
	return (uint16_t)ADC;	// Return calculated voltage.
}

/**
 * Read the ADC value and convert to a voltage. Note that this requires the
 * value of `refVoltage` to be correct.
 *
 * \return	The actual voltage on the ADC.
 */
double readVoltage() {
	unsigned int adcval = readAnalog();
	return (double)adcval / 1024.0 * refVoltage;	// Return calculated voltage.
}

uint16_t* getSpectrum(const Msgeq7 eq) {
	int i;
	uint16_t* sp = malloc(sizeof(uint16_t) * 7);
		
	for (i = 0; i < 7; i++) {
		*eq->strobeport &= ~_BV(eq->strobepin);
		_delay_us(100);
		sp[i] = readAnalog();
		if (sp[i] < 60)
			sp[i] = 0;
		*eq->strobeport |= _BV(eq->strobepin);
		_delay_us(100);
	}
	
	return sp;
}

void updateSpectrum(const Msgeq7 eq, uint16_t* sp) {
	int i;
	
	for (i = 0; i < 7; i++) {
		*eq->strobeport &= ~_BV(eq->strobepin);
		_delay_us(100);
		sp[i] = readAnalog();
		if (sp[i] < 60)
		sp[i] = 0;
		*eq->strobeport |= _BV(eq->strobepin);
		_delay_us(100);
	}
}
