/**
 * \file uart.c
 * \author Ethan Ruffing
 * \author Rob Bossemeyer
 * 
 * \brief Implementation of uart.h
 *
 * This file contains implementations of the functions defined in uart.h.
 * 
 * This file is based on code provided by Dr. Rob Bossemeyer of Grand Valley
 * State University.
 */


#include "uart.h"

FILE uart_output = FDEV_SETUP_STREAM(USART_Transmit, NULL, _FDEV_SETUP_WRITE);
FILE uart_input = FDEV_SETUP_STREAM(NULL, USART_Receive, _FDEV_SETUP_READ);

void USART_Init(unsigned int ubrr) {
	/* set baud rate */
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;

    UCSR0C = _BV(USBS0) | _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data, 2 stop bits */ 
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */    
}

void USART_Transmit(char c, FILE *stream) {
    if (c == '\n') {
        USART_Transmit('\r', stream); /* transmit carriage return with new line */
    }
    /* Wait for empty transmit buffer */
    while ( !( UCSR0A & (1<<UDRE0)) )
    ;
    /* Put data into buffer, sends the data */
    UDR0 = c;
}

char USART_Receive(FILE *stream) {
    /* Wait for data to be received */
    while ( !(UCSR0A & (1<<RXC0)) )
    ;
    /* Get and return received data from buffer */
    return UDR0;
}
