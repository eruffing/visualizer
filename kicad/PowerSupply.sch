EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:switches
LIBS:Visualizer-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 3
Title "Power Supply"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LM7905 U?
U 1 1 5954FB5E
P 5100 3550
F 0 "U?" H 5250 3355 50  0000 C CNN
F 1 "LM7905" H 5100 3750 50  0000 C CNN
F 2 "" H 5100 3550 50  0001 C CNN
F 3 "" H 5100 3550 50  0001 C CNN
	1    5100 3550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5954FB65
P 5100 3900
F 0 "#PWR?" H 5100 3650 50  0001 C CNN
F 1 "GND" H 5100 3750 50  0000 C CNN
F 2 "" H 5100 3900 50  0001 C CNN
F 3 "" H 5100 3900 50  0001 C CNN
	1    5100 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3500 6050 3500
$Comp
L C C?
U 1 1 5954FB72
P 4550 3700
F 0 "C?" H 4575 3800 50  0000 L CNN
F 1 "25u" H 4575 3600 50  0000 L CNN
F 2 "" H 4588 3550 50  0001 C CNN
F 3 "" H 4550 3700 50  0001 C CNN
	1    4550 3700
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5954FB79
P 5550 3700
F 0 "C?" H 5575 3800 50  0000 L CNN
F 1 "25u" H 5575 3600 50  0000 L CNN
F 2 "" H 5588 3550 50  0001 C CNN
F 3 "" H 5550 3700 50  0001 C CNN
	1    5550 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3500 4700 3500
Wire Wire Line
	5100 3800 5100 3900
Wire Wire Line
	4550 3850 5550 3850
Connection ~ 5100 3850
Wire Wire Line
	5550 3050 5550 3550
Connection ~ 5550 3500
Wire Wire Line
	4550 3050 4550 3550
Connection ~ 4550 3500
Text HLabel 4350 3500 0    60   Input ~ 0
VI
Text HLabel 6050 3500 2    60   Output ~ 0
VO
$Comp
L D D?
U 1 1 59550A77
P 5100 3050
F 0 "D?" H 5100 3150 50  0000 C CNN
F 1 "1n4001" H 5100 2950 50  0000 C CNN
F 2 "" H 5100 3050 50  0001 C CNN
F 3 "" H 5100 3050 50  0001 C CNN
	1    5100 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 3050 4950 3050
Wire Wire Line
	5250 3050 5550 3050
$EndSCHEMATC
