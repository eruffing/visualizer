EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:switches
LIBS:Visualizer-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 3
Title "Equalizer"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MSGEQ7 U?
U 1 1 595414C6
P 5800 3100
F 0 "U?" H 5700 3650 50  0000 R CNN
F 1 "MSGEQ7" H 5700 3550 50  0000 R CNN
F 2 "" H 5800 3100 50  0001 C CNN
F 3 "" H 5800 3100 50  0001 C CNN
	1    5800 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3200 6750 3200
$Comp
L C C?
U 1 1 595414CE
P 6300 3550
F 0 "C?" H 6325 3650 50  0000 L CNN
F 1 "0.1u" H 6325 3450 50  0000 L CNN
F 2 "" H 6338 3400 50  0001 C CNN
F 3 "" H 6300 3550 50  0001 C CNN
	1    6300 3550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 595414D5
P 6300 3700
F 0 "#PWR?" H 6300 3450 50  0001 C CNN
F 1 "GND" H 6300 3550 50  0000 C CNN
F 2 "" H 6300 3700 50  0001 C CNN
F 3 "" H 6300 3700 50  0001 C CNN
	1    6300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2400 5800 2600
Wire Wire Line
	4400 2400 5800 2400
$Comp
L C C?
U 1 1 595414DD
P 5950 2400
F 0 "C?" H 5975 2500 50  0000 L CNN
F 1 "0.1u" H 5975 2300 50  0000 L CNN
F 2 "" H 5988 2250 50  0001 C CNN
F 3 "" H 5950 2400 50  0001 C CNN
	1    5950 2400
	0    1    1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 595414E4
P 6100 2400
F 0 "#PWR?" H 6100 2150 50  0001 C CNN
F 1 "GND" H 6100 2250 50  0000 C CNN
F 2 "" H 6100 2400 50  0001 C CNN
F 3 "" H 6100 2400 50  0001 C CNN
	1    6100 2400
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 595414EA
P 5800 3600
F 0 "#PWR?" H 5800 3350 50  0001 C CNN
F 1 "GND" H 5800 3450 50  0000 C CNN
F 2 "" H 5800 3600 50  0001 C CNN
F 3 "" H 5800 3600 50  0001 C CNN
	1    5800 3600
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 595414F0
P 4950 3400
F 0 "R?" V 5030 3400 50  0000 C CNN
F 1 "200k" V 4950 3400 50  0000 C CNN
F 2 "" V 4880 3400 50  0001 C CNN
F 3 "" H 4950 3400 50  0001 C CNN
	1    4950 3400
	0    1    1    0   
$EndComp
$Comp
L C C?
U 1 1 595414F7
P 5200 3650
F 0 "C?" H 5225 3750 50  0000 L CNN
F 1 "33p" H 5225 3550 50  0000 L CNN
F 2 "" H 5238 3500 50  0001 C CNN
F 3 "" H 5200 3650 50  0001 C CNN
	1    5200 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3400 5300 3400
Wire Wire Line
	5200 3500 5200 3400
Connection ~ 5200 3400
$Comp
L GND #PWR?
U 1 1 59541501
P 5200 3800
F 0 "#PWR?" H 5200 3550 50  0001 C CNN
F 1 "GND" H 5200 3650 50  0000 C CNN
F 2 "" H 5200 3800 50  0001 C CNN
F 3 "" H 5200 3800 50  0001 C CNN
	1    5200 3800
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59541507
P 4950 3100
F 0 "C?" H 4975 3200 50  0000 L CNN
F 1 "0.1u" H 4975 3000 50  0000 L CNN
F 2 "" H 4988 2950 50  0001 C CNN
F 3 "" H 4950 3100 50  0001 C CNN
	1    4950 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 3100 5300 3100
Text HLabel 6750 3200 2    60   Output ~ 0
OUT
Wire Wire Line
	4800 3400 4700 3400
Wire Wire Line
	4700 3400 4700 2400
Connection ~ 4700 2400
Wire Wire Line
	4800 3100 4400 3100
Text HLabel 4400 3100 0    60   Input ~ 0
IN
Text HLabel 4400 2400 0    60   Input ~ 0
VS
Wire Wire Line
	6300 2900 6750 2900
Wire Wire Line
	6300 2800 6750 2800
Text HLabel 6750 2800 2    60   Input ~ 0
STROBE
Text HLabel 6750 2900 2    60   Input ~ 0
RESET
$EndSCHEMATC
