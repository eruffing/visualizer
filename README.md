Music Visualizer
================
Copyright 2017 by Ethan Ruffing.

This is a music visualizer system. The project is divided into two parts - the
circuit/PCB design, and the firmware. These parts can be found in the
corresponding folders in this repository.
